#include <iostream>

using namespace std;
typedef unsigned long long ull;
ull limit = 10e10;
ull poriadok(ull a){
    long long ch =0;
    while(a >0){
        ch++;
        a/=10;
    }
    return ch;
}

ull cifra(ull num, ull n){
    for (ull i = 0; i < n; i++){
        num /= 10;
    }
    return num % 10;
}

int main(){
    ull n;
    cout << "Insert: ";
    while (!(cin >> n)){
        cout << "Insert: " <<endl;
        cin.clear();
        fflush(stdin);
    }
    ull i,sum, pred;
    i=0;
    sum=0;
    pred=0;
    while(sum < n){
        i++;
        if(i<limit){
            pred = sum;
            sum += poriadok(i*i);
        }else{
            cout << "Limit(";
            return 0;
        }
    }
    cout <<cifra(i*i, sum - n);
}


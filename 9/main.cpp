#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n;
    cout << "Insert: " <<endl;
    while (!(cin >> n)){
        cout << "Insert: " << endl;
        cin.clear();
        fflush(stdin);
    }
    cout << "Insert numbers: ";
    vector<int> v;
    for(int i = 0; i < n; i++){
        int a;
        while (!(cin >> a)){
            cout << "Wrong!" << endl;
            cin.clear();
            fflush(stdin);
        }
        v.push_back(a);
    }
    set<int> s;
    for(int i = 0; i < v.size(); i++){
        s.insert(v[i]);
    }
    for(auto i: s){
        cout<<i << " ";
    }
    return 0;
}
